Create or replace trigger TG_AFTER_INS_PEDIDOS_RECEBIDOS
  after insert on PEDIDOS_RECEBIDOS
  referencing new as new old as old
  for each row

/* Descrição: Trigger responsavel por realizar validações e tratamentos após a inserção de dados na tabela PEDIDOS_RECEBIDOS;
   Autor: Jean Matsunaga
   Data: 03/09/2020 */

declare
  v_existe_uf_cidade    number;
  v_existe_pedidos_data number;
  v_valor_pedido_data   number(20,2);
  v_num_tentativa       varchar2(50);
  v_status_tentativa    varchar2(5);
  v_processo            varchar2(100);
  v_msg                 varchar2(300);
  v_erro                varchar2(1);
  v_tupla_original      varchar2(500);
begin  
  begin
    /* Processo: Cidades com Pedidos;
       Descrição: Responsavel pelo tratamento das inserções referentes a tabela: cidades_com_pedido; */
    
    -- Controle de Logs;   
    v_processo       := 'Cidades com Pedidos';
    v_tupla_original := 'UF: ' || :new.UF || ', Cidade: ' || :new.cidade;
    v_erro           := 'N';
    
    -- Verifica se ja existe a UF/Cidade na tabela Cidades_com_pedidos;
    select count(1)
      into v_existe_uf_cidade
      from cidades_com_pedidos ccp
     where ccp.uf     = :new.uf
       and ccp.cidade = :new.cidade;
       
    v_processo := v_processo || ' (Inserção)';

    if v_existe_uf_cidade = 0 then -- Nao existe;
      if :new.cidade is not null and :new.uf is not null then 
        -- Insere somente se a Cidade e UF forem informadas;
        insert into cidades_com_pedidos
          (uf,
           cidade)
         values
          (:new.uf,
           :new.cidade);

        -- Define a mensagem de Log;
        v_msg  := 'Sucesso! Inserção realizada com sucesso. UF: ' || :new.uf || ' - Cidade: ' || :new.cidade;

        -- Grava o registro no Log;
        proc_registra_log(sysdate, :new.numpedido, :new.datapedido, v_processo, v_msg, v_erro, v_tupla_original);
      else
        if :new.uf is null and :new.cidade is not null then
          v_msg := 'Falha! O dado referente a UF não foi informado corretamente. Valor informado UF: ' || :new.uf;
          v_erro := 'S';

          -- Grava o registro no Log;
          proc_registra_log(sysdate, :new.numpedido, :new.datapedido, v_processo, v_msg, v_erro, v_tupla_original);
        elsif :new.cidade is null and :new.uf is not null then

          -- Define mensagem de Log;
          v_msg  := 'Falha! O dado referente a cidade não foi informado corretamente. Valor informado Cidade: ' || :new.cidade;
          v_erro := 'S';

          -- Grava o registro no Log;
          proc_registra_log(sysdate, :new.numpedido, :new.datapedido, v_processo, v_msg, v_erro, v_tupla_original);
         
        elsif :new.cidade is null and :new.uf is null then

          -- Define mensagem de erro;
          v_msg  := 'Falha! Os dados referentes a cidade e UF não foram informados corretamente. Valores informados Cidade: ' || :new.cidade || ' UF: ' || :new.uf;
          v_erro := 'S';

          -- Grava o registro no Log;
          proc_registra_log(sysdate, :new.numpedido, :new.datapedido, v_processo, v_msg, v_erro, v_tupla_original);
        end if;
      end if; -- if :new.cidade is not null and :new.uf is not null then
    else

      -- Define mensagem de erro;
      v_msg  := 'Falha! A cidade que esta sendo inserida já existe. Valores informados Cidade: ' || :new.cidade || ' UF: ' || :new.uf;
      v_erro := 'S';
      
      -- Grava o registro no Log;
      proc_registra_log(sysdate, :new.numpedido, :new.datapedido, v_processo, v_msg, v_erro, v_tupla_original);
    end if; -- if v_existe_uf_cidade = 0 then
  end; -- fim validações Cidades com Pedidos;

  begin
  /* Processo: Total Pedidos por Data;
     Descrição: Responsavel pelo tratamento das inserções referentes a tabela: total_pedidos_data; */
     
    -- Controle de logs;
    v_processo       := 'Total Pedidos por Data';
    v_tupla_original := 'Data Pedido: ' || :new.datapedido || ', Total: ' || :new.total;
    v_erro           := 'N';    
  
    -- Verifica se ja existem pedidos nessa data;
    select count(1)
      into v_existe_pedidos_data
      from total_pedidos_data tpd
     where trunc(tpd.data_dia) = trunc(:new.datapedido);

    if v_existe_pedidos_data > 0 then
      v_processo := v_processo || ' (Atualização)';
    
      -- Verifica o valor existente para o dia informado;
      select nvl(tpd.total_vendido,0)
        into v_valor_pedido_data
        from total_pedidos_data tpd
       where trunc(tpd.data_dia) = trunc(:new.datapedido);
   
       if :new.total is not null and :new.datapedido is not null then
         -- Atualiza valor para dia ja existente;
         update total_pedidos_data tpd
            set tpd.total_vendido = v_valor_pedido_data + :new.total
          where trunc(tpd.data_dia) = trunc(:new.datapedido);   
         
         -- Define mensagem de Log;
         v_msg := 'Sucesso! O valor de ' || :new.total || ' foi acrescido no saldo do dia: ' || :new.datapedido || '.';

         -- Grava o registro no Log;
         proc_registra_log(sysdate, :new.numpedido, :new.datapedido, v_processo, v_msg, v_erro, v_tupla_original);
       else
         if :new.total is null and :new.datapedido is not null then

           -- Define mensagem de Log;
           v_msg  := 'Falha! O dado referente ao valor não foi informado corretamente.';
           v_erro := 'S';

           -- Grava o registro no Log;
           proc_registra_log(sysdate, :new.numpedido, :new.datapedido, v_processo, v_msg, v_erro, v_tupla_original);
         elsif :new.datapedido is null and :new.total is not null then

           -- Define mensagem de Log;
           v_msg  := 'Falha! O dado referente a data do pedido não foi informado corretamente.';
           v_erro := 'S';
           
           -- Grava o registro no Log;
           proc_registra_log(sysdate, :new.numpedido, :new.datapedido, v_processo, v_msg, v_erro, v_tupla_original);
         elsif :new.datapedido is null and :new.total is null then

           -- Define mensagem de Log;
           v_msg  := 'Falha! Os dados referentes a data do pedido e o valor do pedido não foram informados corretamente.';
           v_erro := 'S';

           -- Grava o registro no Log;
           proc_registra_log(sysdate, :new.numpedido, :new.datapedido, v_processo, v_msg, v_erro, v_tupla_original);
         end if;
       end if;  
    else
      v_processo := v_processo || ' (Inserção)';

      if :new.total is not null and :new.datapedido is not null then
        -- Insere um novo dia / total;
        insert into total_pedidos_data
            (total_pedidos_data.data_dia,
             total_pedidos_data.total_vendido)
          values
            (to_date(:new.datapedido,'dd/mm/rrrr'),
             :new.total);

        -- Define mensagem de log;
        v_msg := 'Sucesso! O saldo do dia: ' || :new.datapedido || ' foi inserido com o valor de ' || :new.total || '.';

        -- Grava o registro no Log;
        proc_registra_log(sysdate, :new.numpedido, :new.datapedido, v_processo, v_msg, v_erro, v_tupla_original);
      else
        if :new.total is null and :new.datapedido is not null then

          -- Define mensagem de Log;
          v_msg  := 'Falha! O dado referente ao valor não foi informado corretamente.';
          v_erro := 'S';

          -- Grava o registro no Log;
          proc_registra_log(sysdate, :new.numpedido, :new.datapedido, v_processo, v_msg, v_erro, v_tupla_original);
          
        elsif :new.datapedido is null and :new.total is not null then

          -- Define mensagem de Log;
          v_msg  := 'Falha! O dado referente a data do pedido não foi informado corretamente.';
          v_erro := 'S';

          -- Grava o registro no Log;
          proc_registra_log(sysdate, :new.numpedido, :new.datapedido, v_processo, v_msg, v_erro, v_tupla_original);
          
        elsif :new.datapedido is null and :new.total is null then

          -- Define mensagem de Log;
          v_msg  := 'Falha! Os dados referentes a data do pedido e o valor do pedido não foram informados corretamente.';
          v_erro := 'S';

          -- Grava o registro no Log;
          proc_registra_log(sysdate, :new.numpedido, :new.datapedido, v_processo, v_msg, v_erro, v_tupla_original);
        end if;
      end if;
    end if;
  end; -- fim validações Total pedido por Data;

  begin
  /* Processo: Tentativas de Pagamento;
     Descrição: Responsavel pelo tratamento das inserções referentes a tabela: tentativa_pagamento; */
       
    v_processo       := 'Tentativas de Pagamento (Inserção)';
    v_tupla_original := 'Numero do Pedido: ' || :new.numpedido || ', Tentativas Pagamento: ' || :new.tentativas_pagamento;
    v_erro           := 'N';     
    
    -- Expressão Regular dentro do loop para separar as tentativas de pagamento delimitadas por virgula.
    for i in (select trim(regexp_substr(:new.tentativas_pagamento, '[^,]+', 1, level)) tentativas_pagamento
                from dual
               connect by instr(:new.tentativas_pagamento, ',', 1, level - 1) > 0) loop
               
               -- Função que retorna o valor da tentativa e o status de acordo com a posição e o delimitador;
               v_num_tentativa    := func_busca_valor_separador(i.tentativas_pagamento,1,':');
               v_status_tentativa := func_busca_valor_separador(i.tentativas_pagamento,2,':');
             
               if v_num_tentativa is not null and v_status_tentativa is not null then

                 -- Insere a tentativa de pagamento;
                 insert into tentativas_pagamento
                    (numpedido,
                     numtentativa_pagamento,
                     status)
                   values
                     (:new.numpedido,
                      v_num_tentativa,
                      v_status_tentativa);
                      
                 -- Define mensagem de Log;
                 v_msg := 'Sucesso! Inserção da tentativa de pagamento do pedido: ' || :new.numpedido || ' realizada com sucesso. Dados inseridos: Número da tentativa: ' || v_num_tentativa || ', Status: ' || v_status_tentativa || '.';
                 v_erro           := 'N';
                 -- Grava o registro no Log;
                 proc_registra_log(sysdate, :new.numpedido, :new.datapedido, v_processo, v_msg, v_erro, v_tupla_original);
               else
                 if v_num_tentativa is null and v_status_tentativa is not null then

                   -- Define mensagem de Log;
                   v_msg :=  'Falha! Não foi possível inserir a tentativa de pagamento do pedido: ' || :new.numpedido ||' pois o dado referente ao número da tentativa não foi informado corretamente.';
                   v_erro := 'S';
           
                   -- Grava o registro no Log;
                   proc_registra_log(sysdate, :new.numpedido, :new.datapedido, v_processo, v_msg, v_erro, v_tupla_original);

                 elsif v_num_tentativa is not null and v_status_tentativa is null then

                   -- Define mensagem de Log;
                   v_msg  := 'Falha! Não foi possível inserir a tentativa de pagamento do pedido: ' || :new.numpedido ||' pois o dado referente ao status da tentativa não foram informados corretamente.';
                   v_erro := 'S';
           
                   -- Grava o registro no Log;
                   proc_registra_log(sysdate, :new.numpedido, :new.datapedido, v_processo, v_msg, v_erro, v_tupla_original);

                 elsif v_num_tentativa is null and v_status_tentativa is null then

                   -- Define mensagem de Log;
                   v_msg  := 'Falha! Não foi possível inserir a tentativa de pagamento do pedido: ' || :new.numpedido ||' pois os dados referentes ao número da tentativa e o status não foram informados corretamente.';
                   v_erro := 'S';
           
                   -- Grava o registro no Log;
                   proc_registra_log(sysdate, :new.numpedido, :new.datapedido, v_processo, v_msg, v_erro, v_tupla_original);
                 end if;
               end if;
    end loop;
  end; -- fim tratamento Tentativas de Pagamento;
end;