create or replace procedure proc_registra_log(p_data_log       in date,
                                              p_numpedido      in number,
                                              p_data_venda     in date,
                                              p_processo       in varchar2,
                                              p_mensagem       in varchar2,
                                              p_erro           in varchar2,
                                              p_tupla_original in varchar2) is

/* Descrição: Procedimento autonomo responsável pela gravação de logs na tabela logs_processamento_pedido, o motivo de ser uma procedure
              ao invés de uma função se deu por não ser necessário retornar um valor confirmando se o log foi inserido.
   Autor: Jean Matsunaga
   Data: 03/09/2020 - 02:25am */

-- Sessão independente;
PRAGMA AUTONOMOUS_TRANSACTION; 

begin
  insert into logs_processamento_pedido
     (data_log,
      numpedido,
      data_venda,
      texto_operacao,
      resultado_operacao,
      erro,
      tupla_original)
    values
      (p_data_log,
       p_numpedido,
       p_data_venda,
       p_processo,
       p_mensagem,
       p_erro,
       p_tupla_original);

   commit;
   
exception
  when others then
    raise_application_error('-20999','Ocorreu um erro no procedimento proc_registra_log: ' || sqlerrm);
end;
