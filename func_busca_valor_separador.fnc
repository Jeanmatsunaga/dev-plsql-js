create or replace function func_busca_valor_separador(p_linha       in varchar2,
                                                      p_posicao     in number,
                                                      p_delimitador in varchar2) return varchar2 is

/* Descricao: Funcao auxiliar criada para receber uma string, um delimitador, a posicao e retornar a string
              referente a posicao informada;
   Autor: Jean Matsunaga
   Data: 03/09/2020 */

    v_inicial   number; -- Posicao Inicial;
    v_tamanho   number; -- Tamanho
    v_coluna    number;  -- Numero da coluna a retornar
    v_linha     varchar2(1000) := p_linha || p_delimitador;
    v_resultado varchar2(1000) := null;

begin
  if p_linha is not null and length(p_posicao) >= 1 then

    v_tamanho := 0;
    v_coluna := p_posicao;

    loop
      v_coluna := v_coluna - 1;
      v_inicial := v_tamanho + 1;

      v_tamanho := nvl(instr(v_linha, p_delimitador , v_inicial), 1);

      exit when v_coluna <= 0;
    end loop;

    v_resultado := substr(v_linha, v_inicial, (v_tamanho - v_inicial));
  end if;
  
  -- Retorna o valor da posicao informada;
  return v_resultado;

exception
  when others then
    return null;
end;
